<?php


$router->get('/', ['as' => 'home', 'uses' => 'PagesController@index']); // всички форуми и подфоруми
$router->get('/category/{id}', ['as' => 'category', 'uses' => 'PagesController@category']); // конкретна категория

/**
 * Разглеждане на всички или конкретен потребител
 */
$router->get('/members', ['as' => 'members', 'uses' => 'MembersController@index']); // изобразяване на всички потребители
$router->get('/member/{id}', ['as' => 'member', 'uses' => 'MembersController@show']); // изобразяване на конкретен потребител

/**
 * Разглеждане и Добавяне на постове в конкретна тема
 */
$router->get('/topic/{id}', ['as' => 'topic', 'uses' => 'PostsController@show']); // изобразяване на всички постове в конкретна тема
$router->get('/topic/{id}/page/{page}', ['as' => 'topic_page', 'uses' => 'PostsController@show']); // странициране на постовете в тема
$router->get('/topic/{id}/post', ['as' => 'post.create', 'uses' => 'PostsController@create']); // форма за добавяне на нов пост в тема
$router->post('/topic/{id}/post', ['as' => 'post.store', 'uses' => 'PostsController@store']); // създаване на пост в тема
$router->get('/topic/{topic_id}/page/{page}/post/{entry_id}/edit', ['as' => 'post.edit', 'uses' => 'PostsController@edit']); // създаване на пост в тема
$router->post('/topic/{topic_id}/page/{page}/post/{entry_id}/edit', ['as' => 'post.update', 'uses' => 'PostsController@update']); // създаване на пост в тема

/**
 * Разглеждане и Добавяне на теми в конкретен форум
 */
$router->get('/forum/{id}', ['as' => 'forum', 'uses' => 'TopicsController@show']); // изобразяване на всички теми в конкретен форум
$router->get('/forum/{id}/page/{page}', ['as' => 'forum_page', 'uses' => 'TopicsController@show']); // изобразяване на всички теми в конкретен форум
$router->get('/forum/{id}/post', ['as' => 'topic.create', 'uses' => 'TopicsController@create']); // форма за добавяне на нова тема
$router->post('/forum/{id}/post', ['as' => 'topic.store', 'uses' => 'TopicsController@store']); // създаване на нова тема

/**
 * Управление на сесии за Вход/Изход/Регистрация
 */
$router->post('/login', ['as' => 'login', 'uses' => 'SessionsController@login']);
$router->get('/logout', ['as' => 'logout', 'uses' => 'SessionsController@logout']);
$router->post('/register', ['as' => 'register', 'uses' => 'SessionsController@register']);

/**
 * Страници за грешки 
 */
$router->get('/banned', ['as' => 'banned', 'uses' => 'PagesController@banned']);