/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/main.js":
/***/ (function(module, exports) {

$(document).ready(function () {
    var $btnSectionToggle = $('.section-toggle-btn');

    /** SECTION TOGGLING **/
    $btnSectionToggle.on('click', function (e) {
        e.preventDefault();
        var children = $(this).children();
        var section = $(this).parent().parent().next();

        if (children.hasClass('fa-angle-down')) {
            children.removeClass('fa-angle-down');
            children.addClass('fa-angle-left');
            section.slideUp(400);
            return;
        }

        children.removeClass('fa-angle-left');
        children.addClass('fa-angle-down');
        section.slideDown(400);
    });
    /** END SECTION TOGGLING **/

    /** MODAL **/
    var $sidebar = $('#sidebar');
    var $sidebarContent = $('.sidebar-content');
    var $btn = $('a[data-target]');
    var $btnClose = $('.btn-close');
    var modal = null;

    $btn.on('click', function (e) {
        var data = $(this).data('target');
        if (data !== undefined) {
            hideSidebar();
            modal = $('#' + data);
            modal.show();
        }
    });

    $btnClose.on('click', function () {
        if (modal !== null) {
            modal.hide();
        }
    });
    /** END MODAL **/

    /** SIDEBAR **/
    $('#mobile-nav').on('click', function (e) {
        e.preventDefault();

        $sidebar.show(0, function () {
            $sidebarContent.animate({
                right: 0,
                opacity: 1
            }, 400);
        });
    });
    /** END SIDEBAR **/

    var $btnPostContextMenu = $('.post-context-menu');

    /** SECTION TOGGLING **/
    $btnPostContextMenu.on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        hideContextMenus();

        var contextMenu = $(this).next('.dropdown-content');
        if (contextMenu.hasClass('show')) {
            contextMenu.removeClass('show');
        } else {
            contextMenu.addClass('show');
        }
    });
    /** END SECTION TOGGLING **/

    $(window).on('click', function (e) {
        if (e.target.className === 'modal') {
            modal.hide();
        }

        if (e.target.id === 'sidebar') {
            hideSidebar();
        }

        hideContextMenus();
    });

    $('#bottom-close-btn').on('click', function () {
        $sidebarContent.animate({
            right: '-80%',
            opacity: 0
        }, 400, function () {
            $sidebar.hide();
        });
    });

    function hideSidebar() {
        $sidebar.hide(0, function () {
            $sidebarContent.css({
                right: '-80%',
                opacity: 0
            });
        });
    }

    function hideContextMenus() {
        var $elDropdownContent = $('.dropdown-content');

        for (var _i = 0; _i < $elDropdownContent.length; _i++) {
            if ($elDropdownContent.eq(_i).hasClass('show')) {
                $elDropdownContent.eq(_i).removeClass('show');
            }
        }
    }

    var x = void 0,
        i = void 0,
        j = void 0,
        selElmnt = void 0,
        a = void 0,
        b = void 0,
        c = void 0;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("mobile-breadcrumbs");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 0; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y = void 0,
                    i = void 0,
                    k = void 0,
                    s = void 0,
                    h = void 0;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        window.location = s.options[i].value;
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x = void 0,
            y = void 0,
            i = void 0,
            arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i);
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);

    $('#new-post-form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: window.location.href,

            data: {
                title: $('input[name=title]').val(),
                description: $('input[name=description]').val(),
                post: CKEDITOR.instances.editor.getData()
            },
            success: function success(data, textStatus, xhr) {
                if (xhr.status === 200) {
                    window.location = data;
                }
            },
            statusCode: {
                429: function _() {
                    alert('Опа, не бързай толкова!');
                }
            }
        });
    });
});

/***/ }),

/***/ "./resources/assets/sass/layout.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./resources/assets/js/main.js");
module.exports = __webpack_require__("./resources/assets/sass/layout.scss");


/***/ })

/******/ });