/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for a single toolbar row.
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'forms' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'tools' },
		{ name: 'others' },
		{ name: 'about' },
		{name: 'pbckcode'},
	];

	config.removePlugins = 'about,sourcearea';
	config.extraPlugins = 'bbcode,codesnippet';

    config.codeSnippet_theme = 'github';
    config.codeSnippet_codeClass = 'hljs';

    config.fontSize_sizes = "1pt/8pt;7pt/14pt;16pt/23pt";

	// PBCKCODE CUSTOMIZATION
    config.pbckcode = {
        // An optional class to your pre tag.
        cls: '',

        // The syntax highlighter you will use in the output view
        highlighter: 'HIGHLIGHT',

        // An array of the available modes for you plugin.
        // The key corresponds to the string shown in the select tag.
        // The value correspond to the loaded file for ACE Editor.
        modes: [
            ['Assembler', 'assembly_x86'],
            ['C/C++', 'c_cpp'],
            ['C9Search', 'c9search'],
            ['Clojure', 'clojure'],
            ['CoffeeScript', 'coffee'],
            ['ColdFusion', 'coldfusion'],
            ['C#', 'csharp'],
            ['CSS', 'css'],
            ['Diff', 'diff'],
            ['Glsl', 'glsl'],
            ['Go', 'golang'],
            ['Groovy', 'groovy'],
            ['haXe', 'haxe'],
            ['HTML', 'html'],
            ['Jade', 'jade'],
            ['Java', 'java'],
            ['JavaScript', 'javascript'],
            ['JSON', 'json'],
            ['JSP', 'jsp'],
            ['JSX', 'jsx'],
            ['LaTeX', 'latex'],
            ['LESS', 'less'],
            ['Liquid', 'liquid'],
            ['Lua', 'lua'],
            ['LuaPage', 'luapage'],
            ['Markdown', 'markdown'],
            ['OCaml', 'ocaml'],
            ['Perl', 'perl'],
            ['pgSQL', 'pgsql'],
            ['PHP', 'php'],
            ['Powershell', 'powershel1'],
            ['Python', 'python'],
            ['R', 'ruby'],
            ['OpenSCAD', 'scad'],
            ['Scala', 'scala'],
            ['SCSS/Sass', 'scss'],
            ['SH', 'sh'],
            ['SQL', 'sql'],
            ['SVG', 'svg'],
            ['Tcl', 'tcl'],
            ['Text', 'text'],
            ['Textile', 'textile'],
            ['XML', 'xml'],
            ['XQuery', 'xq'],
            ['YAML', 'yaml'],
        ],

        // The theme of the ACE Editor of the plugin.
        theme: 'textmate',

        // Tab indentation (in spaces)
        tab_size: '4'
    };

    // The default plugins included in the basic setup define some buttons that
    // are not needed in a basic editor. They are removed here.
    config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Strike,Subscript,Superscript';

    // Dialog windows are also simplified.
    config.removeDialogTabs = 'link:advanced';
};
