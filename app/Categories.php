<?php
namespace App;

class Categories extends AbstractRequest
{
    const URI = 'categories';

    private $collection;

    public function __construct() {
        parent::__construct();
        $this->makeRequest('GET', $this->buildHttpRequest(Categories::URI));

        $this->collection = collect($this->getBody());
    }

    public function getBody(): array {
        return $this->curl->response;
    }

    public function getList() {
        return $this->collection->map(function ( $category ) {
            return $this->makeCategory($category->id);
        });
    }

    public function getById($id) {
        return $this->makeCategory($this->collection->firstWhere('id', $id));
    }

    private function makeCategory($category) {
        return new Category($category);
    }
}