<?php
namespace App;

use Curl\Curl;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

abstract class AbstractRequest extends AbstractCookies
{
    protected static $BASE_URI;
    protected $curl;

    protected function __construct() {
        AbstractRequest::$BASE_URI = env('API_URI');

        try {
            $this->curl = new Curl();
        } catch( \ErrorException $e ) {
            Log::error("Curl Error: " . $e->getMessage());
        }

        $this->setUserIp();
        $this->setUserAgent();
        $this->setFollowLocation();
    }

    protected function makeRequest(string $type, string $uri, array $params = []): void {
        $type = strtolower($type);

        if ( !is_null($this->getCookie('session_id')) ) {
            $this->curl->setCookie('session_id', $this->getCookie('session_id'));
        }

        if ( $this->getCookie('anonlogin') > 0 ) {
            $this->curl->setCookie('anonlogin', $this->getCookie('anonlogin'));
        }

        call_user_func([$this, $type], $uri, $params);

        session_regenerate_id(true);
        $this->setCookies($this->curl->getResponseCookies());
        $this->close();
    }

    protected function setMemberId($member_id) {
        $this->setCookie('member_id', $member_id);
    }

    protected function buildHttpQuery( array $args ): string {
        return implode('/', $args);
    }

    protected function buildHttpRequest($uri): string {
        if ( is_array($uri) ) {
            $uri = $this->buildHttpQuery($uri);
        }
        return $this->buildHttpQuery([static::$BASE_URI, $uri]);
    }

    protected function getHttpStatusCode() {
        return $this->curl->httpStatusCode;
    }

    private function get($uri, $params) {
        $this->curl->get($uri, $params);
    }

    private function post($uri, $params) {
        $this->curl->post($uri, $params);
    }

    private function setFollowLocation() {
        $this->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
    }

    private function setUserAgent() {
        $this->curl->setUserAgent(Request::userAgent());
    }

    private function setUserIp() {
        $this->curl->setHeader('X-Forwarded-For', $_SERVER['REMOTE_ADDR']);
    }

    private function extractResponseCookies() {
        $response_headers = $this->curl->rawResponseHeaders;
        preg_match_all('/Set-Cookie:\s*(.*)/', $response_headers, $headers);
        Log::info('Response cookies: ', $headers);
        foreach ( $headers[0] as $header) {
            header($header);
        }
    }

    private function close() {
        $this->curl->close();
    }

    abstract public function getBody();
}