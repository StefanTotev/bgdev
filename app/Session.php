<?php
namespace App;

class Session extends AbstractRequest
{
    const LOGIN_URI = 'login';
    const LOGOUT_URI = 'logout';
    const REGISTER_URI = 'register';

    public function __construct() {
        parent::__construct();
    }

    public function login($params) {
        $this->makeRequest('POST', $this->buildHttpRequest(Session::LOGIN_URI), $params);
    }

    public function register($params) {
        $this->makeRequest('POST', $this->buildHttpRequest(Session::REGISTER_URI), $params);
    }

    public function logout() {
        $this->makeRequest('GET', $this->buildHttpRequest(Session::LOGOUT_URI));
    }

    public function getBody() {
        return $this->curl->response;
    }
}