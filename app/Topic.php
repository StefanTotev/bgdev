<?php
namespace App;

use Illuminate\Support\Collection;

class Topic extends AbstractRequest
{
    const URI = 'topic';

    private $collection;

    public function __construct(int $topic_id, int $page = 1) {
        parent::__construct();
        if ( $page > 1 ) {
            $query_string = $this->buildHttpRequest([Topic::URI, $topic_id, 'page', $page]);
        } else {
            $query_string = $this->buildHttpRequest([Topic::URI, $topic_id]);
        }

        $this->makeRequest('GET', $query_string);

        $this->collection = $this->getBody();
    }

    public function getBody(): Collection {
        return collect($this->curl->response);
    }

    public function getId() {
        return $this->getDetails('id');
    }

    public function getName() {
        return htmlspecialchars_decode($this->getDetails('title'), ENT_QUOTES);
    }

    public function getDetails($needle) {
        return $this->collection->get('topic')->{$needle};
    }

    public function getPosts() {
        return collect($this->collection->get('posts'));
    }

    public function getEntry($entry_id) {
        return $this->getPosts()->firstWhere('id', $entry_id);
    }

    public function getTotalPosts() {
        return $this->getDetails('posts');
    }

    public function getForum() {
        return new Forum($this->getDetails('forum_id'));
    }
}