<?php
namespace App;

use Illuminate\Support\Collection;

class Forum extends AbstractRequest
{
    const URI = 'forum';

    public $collection;
    public $category;
    public $forum_id;

    public function __construct(int $forum_id, $page = 1) {
        parent::__construct();
        if ( $page > 1 ) {
            $query_string = $this->buildHttpRequest([Forum::URI, $forum_id, 'page', $page]);
        } else {
            $query_string = $this->buildHttpRequest([Forum::URI, $forum_id]);
        }

        $this->makeRequest('GET', $query_string);

        $this->forum_id = $forum_id;
        $this->collection = $this->getBody();
    }

    public function getBody(): Collection {
        return collect($this->curl->response);
    }

    public function getId() {
        return $this->collection->get('forum')->id;
    }

    public function getName(): string {
        return $this->collection->get('forum')->name;
    }

    public function getTopics(): array {
        return $this->collection->get('topics');
    }

    public function getSubForums(): array {
        return $this->collection->get('sub_forums');
    }

    public function getCategory(): Category {
        return new Category($this->collection->get('forum')->category_id);
    }

    public function getRecentTopic() {
        return $this->getTopics()[0];
    }
}