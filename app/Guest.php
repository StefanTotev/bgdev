<?php
namespace App;


use App\Helpers\Color;
use Carbon\Carbon;

class Guest extends AbstractUser
{
    public function getId() {
        return 0;
    }

    public function getName() {
        return 'Guest';
    }

    public function getTitle() {
        return 'Guest';
    }

    public function getGroup() {
        return 2;
    }

    public function getEmail() {
        return '';
    }

    public function getJoined() {
        return Carbon::createFromTimestamp(0)->format('d-m-Y');
    }

    public function getAvatar() {
        return '/images/avatars/avatar1.png';
    }

    public function getSignature() {
        return '';
    }

    public function getPosts() {
        return 0;
    }

    public function getLastPost() {
        return Carbon::createFromTimestamp(0)->format('j M Y');
    }

    public function getLastVisit() {
        return Carbon::createFromTimestamp(0)->format('j M Y');
    }

    public function getLastActivity() {
        return Carbon::createFromTimestamp(0)->format('j M Y');
    }

    public function getColor() {
        return Color::get($this->getGroup());
    }

    public function isLogged() {
        return false;
    }
}