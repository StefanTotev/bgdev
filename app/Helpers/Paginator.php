<?php
namespace App\Helpers;


class Paginator
{
    const MARGIN = 2;

    private $current_page;
    private $first_page;
    private $per_page;
    private $total_items;

    private $output;

    public function __construct() {
        $this->current_page = 1;
        $this->first_page = 1;
        $this->output = '';
    }

    public function setItems(int $total_items, $per_page) {
        $this->total_items = $total_items;
        $this->setPerPage($per_page);
    }

    public function setPerPage(int $per_page) {
        $this->per_page = $per_page;
    }

    public function setCurrent(int $page) {
        $this->current_page = $page;
    }

    public function count() {
        return ceil($this->total_items / $this->per_page);
    }

    public function first() {
        return $this->first_page;
    }

    public function last() {
        return $this->count();
    }

    public function current() {
        return $this->current_page;
    }

    public function next() {
        if ( $this->current() < $this->last() )
            return $this->current() + 1;
        return $this->current();
    }

    public function previous() {
        if ( $this->current() > $this->first() )
            return $this->current() - 1;
        return $this->current();
    }

    public function isRequestedPageAvailable() {
        return ($this->current() > 0 && $this->current() <= $this->count());
    }

    public function start() {
        $start = $this->current() - Paginator::MARGIN;
        $start = $start < 1 ? 1 : $start;

        return $start;
    }

    public function end() {
        $end = $this->current() + Paginator::MARGIN;
        $end = $end > $this->count() ? $this->count() : $end;

        return $end;
    }

    public function render($topic_id) {
        $start = $this->current() - Paginator::MARGIN;
        $start = $start < 1 ? 1 : $start;
        $end = $this->current() + Paginator::MARGIN;
        $end = $end > $this->count() ? $this->count() : $end;

        for ( $i = $start; $i <= $end; ++$i ) {
            if ( $i == $this->current() ) {
                $this->output .= "<li><a href=\"". route('topic_page', ['id' => $topic_id, 'page' => $i]) ."\">{$i}</a></li>";
            } else {
                $this->output .= "<li><a href=\"". route('topic_page', ['id' => $topic_id, 'page' => $i]) ."\">{$i}</a></li>";
            }

        }

        return $this->output;
    }
}