<?php
namespace App\Helpers;


class Breadcrumbs
{
    private static $breadcrumbs = [];

    public function set($url, $id, $name) {
        array_push(static::$breadcrumbs, [
            'url' => $url,
            'id' => $id,
            'name' => $name
        ]);
    }

    public function get() {
        return static::$breadcrumbs;
    }
}