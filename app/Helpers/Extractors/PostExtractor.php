<?php
namespace App\Helpers\Extractors;


use Carbon\Carbon;
use Sunra\PhpSimple\HtmlDomParser;

class PostExtractor
{
    private $post;
    private $output;

    public function __construct(string $post) {
        $this->post = $this->clearTags($post);
        $this->post = HtmlDomParser::str_get_html($this->post);
    }

    public function extract() {
        $translate = $this->translateCodes();
        $translate = $this->translateQuotes();
        $this->output = $translate->save();

        return $this->output;
    }

    public function extractToBBCode() {
        $translate = $this->codeToBBCode();
        $translate = $this->quoteToBBCode();
        $this->output = $translate->save();

        return $this->output;
    }

    private function translateQuotes() {
        $this->clearComments();

        $tables = $this->post->find('table');
        for ( $i = sizeof($tables)-1; $i >= 0; $i-- ) {
            if ( $quote = $tables[$i]->find("td[id='QUOTE']",0) ) {
                $quote_info = '';

                if ( preg_match('/\(\s*\\"?(.*?)\s*\\"?\)/s', $quote->parent->parent->children(0)->innertext, $match) ) {
                    $split = explode("@", $match[1]);
                    $author = trim($split[0]);
                    $quote_info .= $author;

                    if ( isset($split[1]) ) {
                        $date = trim($split[1]);
                        $date = Carbon::createFromTimeString($date)->format('d-m-Y, H:i');

                        $quote_info .= ' @ ' . $date;
                    }
                }


                $tables[$i]->outertext = <<<HTML
                    <div class="quote">
                        <div class="quote-info">$quote_info</div>
                        <div class="quote-body">
                            $quote->innertext
                        </div>
                    </div>
HTML;
            }
        }

        return $this->post;
    }

    private function translateCodes() {
        $this->clearComments();

        $tables = $this->post->find('table');
        for ( $i = sizeof($tables)-1; $i >= 0; $i-- ) {
            if ( $code = $tables[$i]->find("td[id='CODE']",0) ) {
                $tables[$i]->outertext = "<pre><code class='hljs'>" . $code->innertext ."</code></pre>";
            }
        }

        return $this->post;
    }

    public function quoteToBBCode() {
        $this->clearComments();

        $tables = $this->post->find('table');
        for ( $i = sizeof($tables)-1; $i >= 0; $i-- ) {
            if ( $quote = $tables[$i]->find("td[id='QUOTE']",0) ) {
                $bbcode = '[quote';

                if ( preg_match('/\((.*?)\)/s', $quote->parent->parent->children(0)->innertext, $match) ) {
                    $split = explode("@", $match[1]);
                    $author = trim($split[0]);
                    $bbcode .= '='.$author;

                    if ( isset($split[1]) ) {
                        $date = trim($split[1]);
                        $date = Carbon::createFromTimeString($date)->format('d-m-Y, H:i');
                        $bbcode .= ','.$date;
                    }
                }
                $bbcode .= ']';
                $bbcode .= $quote->innertext;
                $bbcode .= '[/quote]';

                $tables[$i]->outertext = $bbcode;
            }
        }

        return $this->post;
    }

    public function codeToBBCode() {
        $this->clearComments();

        $tables = $this->post->find('table');
        for ( $i = sizeof($tables)-1; $i >= 0; $i-- ) {
            if ( $code = $tables[$i]->find("td[id='CODE']",0) ) {
                $tables[$i]->outertext = "[code]" . html_entity_decode($code->innertext) ."[/code]";
            }
        }

        return $this->post;
    }

    private function clearComments() {
        $this->post->set_callback(function ($element) {
            if ( $element->tag == 'comment' ) {
                $element->outertext = '';
            }
        });
    }

    private function clearTags($post) {
        return preg_replace('/<\/?div.*?>/', '', $post);
    }

    public function __toString() {
        return $this->output;
    }
}