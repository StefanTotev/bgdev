<?php
namespace App\Helpers;


final class Color
{
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const VIP = 'vip';
    const MEMBER = 'member';
    const GUEST = 'red';

    public static function get($group) {
        switch($group) {
            case 2: $color = Color::GUEST; break;
            case 4: $color = Color::ADMIN; break;
            case 10: $color = Color::MODERATOR; break;
            case 11: $color = Color::VIP; break;
            default: $color = Color::MEMBER; break;
        }

        return $color;
    }
}