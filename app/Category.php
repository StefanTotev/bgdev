<?php
namespace App;

use Illuminate\Support\Collection;

class Category extends AbstractRequest
{
    const URI = 'category';

    private $collection;

    public function __construct($id) {
        parent::__construct();
        $this->makeRequest('GET', $this->buildHttpRequest([Category::URI, $id]));

        $this->collection = $this->getBody();
    }

    public function getBody(): Collection {
        return collect($this->curl->response);
    }

    public function getId(): int {
        return $this->collection->get('category')->id;
    }

    public function getName(): string {
        return $this->collection->get('category')->name;
    }

    public function getDescription(): string {
        return $this->collection->get('category')->description;
    }

    public function getForums() {
        return $this->collection->get('forums');
    }
}