<?php
namespace App;


class Status extends AbstractRequest
{
    const URI = 'status';

    private $members;

    public function __construct(Members $members) {
        parent::__construct();
        $this->makeRequest('GET', $this->buildHttpRequest(Status::URI));
        $this->setMemberId($this->user()->member_id);

        $this->members = $members;
    }

    public function getBody() {
        return $this->curl->response;
    }

    public function getCurrentMember() {
        return $this->user();
    }

    private function user() {
        return $this->getBody()->user;
    }

    public function getMembers() {
        return $this->members;
    }

    public function getOnlineData() {
        $users = $this->getBody()->online;

        $usersData = [
            'anon' => 0,
            'guests' => 0,
            'members' => []
        ];

        foreach ($users as $user) {
            if ( $user->member_id == 0 ) {
                if ( $user->member_name == 'Annon' ) {
                    $usersData['anon'] += $user->members_count;
                } else {
                    $usersData['guests'] += $user->members_count;
                }
            } else {
                array_push($usersData['members'], $this->members->getMemberById($user->member_id));
            }
        }

        return $usersData;
    }

}