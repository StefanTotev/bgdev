<?php
namespace App\ViewComposers;

use App\Guest;
use App\Members;
use App\Status;
use Illuminate\View\View;

class StatusComposer
{
    protected $status;

    public function __construct(Status $status) {
        $this->status = $status;
    }

    public function compose(View $view) {
        $view->with('status', $this->status->getBody())
                ->with('online', $this->status->getOnlineData());

        $current_user = $this->status->getCurrentMember();
        if ( $current_user->member_id > 0 ) {
            $view->with('member', (new Members())->getMemberById($current_user->member_id));
        } else {
            $view->with('member', new Guest($current_user));
        }
    }
}