<?php
namespace App\ViewComposers;

use App\Members;
use Illuminate\View\View;

class MembersComposer
{
    protected $members;

    public function __construct(Members $members) {
        $this->members = $members;
    }

    public function compose(View $view) {
        $view->with('members', $this->members);
    }
}