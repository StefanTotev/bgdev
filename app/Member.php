<?php
namespace App;

use App\Helpers\Color;
use Carbon\Carbon;

class Member extends AbstractUser
{
    public function getId() {
        return $this->collection->id;
    }

    public function getName() {
        return $this->collection->name;
    }

    public function getTitle() {
        return $this->collection->title;
    }

    public function getGroup() {
        return $this->collection->mgroup;
    }

    public function getEmail() {
        return $this->collection->email;
    }

    public function getJoined() {
        return Carbon::createFromTimestamp($this->collection->joined)->format('d-m-Y');
    }

    public function getAvatar() {
        return ($this->collection->avatar == 'noavatar' || empty($this->collection->avatar) )
            ? '/images/avatars/avatar1.png'
            : $this->collection->avatar;
    }

    public function getSignature() {
        return ( empty($this->collection->signature) )
            ? false
            : $this->collection->signature;
    }

    public function getPosts() {
        return $this->collection->posts;
    }

    public function getLastPost() {
        return Carbon::createFromTimestamp($this->collection->last_post)->format('j M Y');
    }

    public function getLastVisit() {
        return Carbon::createFromTimestamp($this->collection->last_visit)->format('j M Y');
    }

    public function getLastActivity() {
        return Carbon::createFromTimestamp($this->collection->last_activity)->format('j M Y');
    }

    public function getColor() {
        return Color::get($this->getGroup());
    }

    public function isLogged() {
        return true;
    }
}