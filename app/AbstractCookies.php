<?php
namespace App;

session_start();

abstract class AbstractCookies
{
    protected function setCookie($name, $value): void {
        $isHttpOnly = ($name == 'session_id') ? true : false;
        setcookie($name, $value, 0, '/', '', false, $isHttpOnly);
        $_SESSION[$name] = $value;
    }

    protected function setCookies(array $cookies) {
        foreach ( $cookies as $name => $value ) {
            $this->setCookie($name, $value);
        }
    }

    protected function getCookie($name) {
        if ( isset($_COOKIE[$name]) ) {
            return $_COOKIE[$name];
        } else {
            if ( isset($_SESSION[$name]) ) {
                return $_SESSION[$name];
            } else {
                return null;
            }
        }
    }
}