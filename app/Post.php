<?php
namespace App;


class Post extends AbstractRequest
{
    const URI = 'post';

    const NEW_TOPIC = '01';
    const NEW_POST = '03';
    const EDIT_POST = '09';
    const NEW_PM = '04';

    private $params;

    public function __construct($params, $code) {
        parent::__construct();

        $this->params = array_merge($params, [
            'CODE' => $code,
            'act' => 'Post',
            's' => ' ',
            'bbmode' => 'normal',
            'ffont' => '0',
            'fcolor' => '0',
            'tagcount' => '0',
            'enableemo' => 'yes',
            'enablesig' => 'yes',
            'iconid' => '0'
        ]);
    }

    public function send() {
        $this->makeRequest('POST', $this->buildHttpRequest(Post::URI), $this->params);

        return $this->getHttpStatusCode();
    }

    public static function encode($content) {
        return iconv('UTF-8', 'windows-1251', $content);
    }

    public function getBody() {}
}