<?php
namespace App;

abstract class AbstractUser implements UserInterface
{
    protected $collection;

    public function __construct($collection) {
        $this->collection = $collection;
    }

    abstract public function isLogged();
}