<?php
namespace App;


interface UserInterface
{
    public function getId();
    public function getName();
    public function getGroup();
}