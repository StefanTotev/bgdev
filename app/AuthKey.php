<?php
namespace App;


class AuthKey extends AbstractRequest
{
    const URI = 'authkey';

    public function __construct() {
        parent::__construct();
        $this->makeRequest('GET', $this->buildHttpRequest(AuthKey::URI));
    }

    public function getBody() {
        return $this->curl->response->auth_key;
    }
}