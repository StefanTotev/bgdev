<?php
namespace App;

use Illuminate\Support\Collection;

class Members extends AbstractRequest
{
    const URI = 'members';

    public static $collection;

    public function __construct() {
        parent::__construct();
        $this->makeRequest('GET', $this->buildHttpRequest(Members::URI));

        self::$collection = $this->getBody();
    }

    public function getBody(): Collection {
        return collect($this->curl->response);
    }

    public function getAll(): array {
        return $this->getBody()->toArray();
    }

    public function getMemberById($id): AbstractUser {
        if ( $id <= 0 ) {
            return new Guest([]);
        }
        return new Member(self::$collection->firstWhere('id', $id));
    }

    public function getMemberByName($name): AbstractUser {
        if ( $name == 'Guest' ) {
            return new Guest([]);
        }
        return new Member(self::$collection->firstWhere('name', $name));
    }
}