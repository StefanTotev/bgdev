<?php
namespace App\Http\Middleware;


use App\Guest;
use App\Status;
use Closure;

class AuthMiddleware
{
    protected $member;
    protected $status;

    public function __construct(Status $status) {
        $this->member = $status->getMembers()->getMemberById($status->getCurrentMember()->member_id);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $this->member instanceof Guest) {
            return redirect()->to($_SERVER['HTTP_REFERER']);
        }

        return $next($request);
    }
}