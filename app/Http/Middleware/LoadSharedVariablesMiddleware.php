<?php
namespace App\Http\Middleware;

use App\Guest;
use App\Members;
use App\Status;
use Closure;
use Illuminate\Support\Facades\View;
use function redirect;

class LoadSharedVariablesMiddleware
{
    protected $status;
    protected $members;

    public function __construct(Status $status, Members $members) {
        $this->status = $status;
        $this->members = $members;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        View::share('status', $this->status->getBody());
        View::share('online', $this->status->getOnlineData());
        View::share('members', $this->members);

        $current_user = $this->status->getCurrentMember();
        if ( $current_user->member_id > 0 ) {
            $member = $this->members->getMemberById($current_user->member_id);
        } else {
            $member = new Guest($current_user);
        }

        View::share('member', $member);

        $request->attributes->set('member', $member);

        return $next($request);
    }
}
