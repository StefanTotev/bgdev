<?php
namespace App\Http\Middleware;


use Closure;

class CheckForBanMiddleware
{
    public function handle($request, Closure $next)
    {
        $member = $request->attributes->get('member');

        if ( $member->getGroup() == 0 ) {
            return redirect()->route('banned');
        }

        return $next($request);
    }
}