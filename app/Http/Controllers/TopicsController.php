<?php
namespace App\Http\Controllers;

use App\AuthKey;
use App\Forum;
use App\Post;
use Illuminate\Http\Request;

class TopicsController extends BaseController
{
    public function show($id, $page = 1) {
        $forum = new Forum($id, $page);
        $category = $forum->getCategory();

        $this->breadcrumbs->set('category', $category->getId(), $category->getName());
        $this->breadcrumbs->set('#', null, $forum->getName());

        $this->paginator->setItems(count($forum->getTopics()), (int)env('TOPICS_PER_PAGE'));
        $this->paginator->setCurrent($page);

        if ( !$this->paginator->isRequestedPageAvailable() ) {
            return redirect()->route('forum', ['id' => $id]);
        }

        return view('topics.show', [
            'forum' => $forum,
            'breadcrumbs' => $this->breadcrumbs->get(),
            'paginator' => $this->paginator
        ]);
    }

    public function create($forum_id) {
        if ( !$this->can() ) {
            return redirect()->to($_SERVER['HTTP_REFERER']);
        }

        $forum = new Forum($forum_id);
        $category = $forum->getCategory();

        $this->breadcrumbs->set('category', $category->getId(), $category->getName());
        $this->breadcrumbs->set('forum', $forum->getId(), $forum->getName());

        return view('topics.create', [
            'forum' => $forum,
            'breadcrumbs' => $this->breadcrumbs->get()
        ]);
    }

    public function store(Request $request, $forum_id) {
        if ( !$this->can() ) {
            return redirect()->to($_SERVER['HTTP_REFERER']);
        }

        $params = [
            'st' => 0,
            'f' => $forum_id,
            'auth_key' => (new AuthKey())->getBody(),
            'TopicTitle' => Post::encode($request->input('title')),
            'TopicDesc' => Post::encode($request->input('description')),
            'Post' => Post::encode($request->input('post')),
        ];

        $post = new Post($params, Post::NEW_TOPIC);
        if ($post->send() == 429) {
            return response('Моля, изчакайте преди да пуснете следващото си съобщение!', 429);
        }

        return response(route('forum', ['id' => $forum_id]), 200);
    }
}