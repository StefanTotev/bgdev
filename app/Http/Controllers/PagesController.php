<?php
namespace App\Http\Controllers;

use App\Category;
use App\Forum;
use App\Topic;

class PagesController extends BaseController
{
    public function index() {
        return view('index');
    }

    public function category($id) {
        $category = new Category($id);

        $this->breadcrumbs->set('category', $id, $category->getName());

        return view('categories.show', [
            'category' => $category,
            'breadcrumbs' => $this->breadcrumbs->get()
        ]);
    }

    public function member($id) {
        $member = $this->members->getMemberByName(urldecode($id));
        return $member->getName();
    }

    public function banned() {
        return view('errors.banned');
    }
}
