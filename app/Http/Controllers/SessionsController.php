<?php
namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    private $session;

    public function __construct(Session $session) {
        $this->session = $session;
    }

    public function login(Request $req) {
        $params =  [
            'UserName' => $req->get('username'),
            'PassWord' => $req->get('password'),
            'CookieDate' => ( !is_null($req->get('rememberme')) ) ? 1 : 0,
            'Privacy' => ( !is_null($req->get('privacy')) ) ? 1 : 0
        ];

        $this->session->login($params);

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function register(Request $req) {

    }

    public function logout() {
        $this->session->logout();

        return redirect($_SERVER['HTTP_REFERER']);
    }
}