<?php
namespace App\Http\Controllers;


use App\Guest;
use App\Helpers\Breadcrumbs;
use App\Helpers\Paginator;
use App\Status;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    protected $breadcrumbs;
    protected $status;
    protected $members;
    protected $paginator;
    protected $member;

    public function __construct(Breadcrumbs $breadcrumbs, Status $status, Paginator $paginator) {
        $this->breadcrumbs = $breadcrumbs;
        $this->status = $status;
        $this->members = $status->getMembers();
        $this->paginator = $paginator;

        $this->setSharedVariables();

//        $this->middleware('banned', ['except' => 'banned']);
    }

    private function setSharedVariables() {
        View::share('status', $this->status->getBody());
        View::share('online', $this->status->getOnlineData());
        View::share('members', $this->members);

        $current_member = $this->status->getCurrentMember();
        if ( $current_member->member_id > 0 ) {
            $member = $this->members->getMemberById($current_member->member_id);
        } else {
            $member = new Guest($current_member);
        }

        $this->member = $member;

        View::share('member', $member);
    }

    protected function can() {
        return !($this->member instanceof Guest );
    }

}