<?php
namespace App\Http\Controllers;

use App\AuthKey;
use App\Helpers\Breadcrumbs;
use App\Helpers\Extractors\PostExtractor;
use App\Helpers\Paginator;
use App\Post;
use App\Status;
use App\Topic;
use Illuminate\Http\Request;
use const JSON_UNESCAPED_UNICODE;
use function response;

class PostsController extends BaseController
{
    public function show($id, $page = 1) {
        $topic = new Topic($id, $page);
        $forum = $topic->getForum();
        $category = $forum->getCategory();

        $this->breadcrumbs->set('category', $category->getId(), $category->getName());
        $this->breadcrumbs->set('forum', $forum->getId(), $forum->getName());

        $this->paginator->setItems(($topic->getTotalPosts()+1), (int)env('POSTS_PER_PAGE'));
        $this->paginator->setCurrent($page);

        if ( !$this->paginator->isRequestedPageAvailable() ) {
            return redirect()->route('topic', ['id' => $id]);
        }

        return view('posts.show', [
            'topic' => $topic,
            'breadcrumbs' => $this->breadcrumbs->get(),
            'paginator' => $this->paginator
        ]);
    }

    public function create($topic_id) {
        if ( !$this->can() ) {
            return redirect()->to($_SERVER['HTTP_REFERER']);
        }

        $topic = new Topic($topic_id);
        $forum = $topic->getForum();
        $category = $forum->getCategory();

        $this->breadcrumbs->set('category', $category->getId(), $category->getName());
        $this->breadcrumbs->set('forum', $forum->getId(), $forum->getName());

        return view('posts.create', [
            'topic' => $topic,
            'breadcrumbs' => $this->breadcrumbs->get()
        ]);
    }

    public function store(Request $request, $topic_id) {
        if ( !$this->can() ) {
            return redirect()->to($_SERVER['HTTP_REFERER']);
        }

        $topic = new Topic($topic_id);

        $params = [
            'st' => 0,
            'f' => $topic->getDetails('forum_id'),
            't' => $topic->getId(),
            'auth_key' => (new AuthKey())->getBody(),
            'Post' => Post::encode($request->input('post')),
        ];

        $post = new Post($params, Post::NEW_POST);
        if ($post->send() == 429) {
            return response('Моля, изчакайте преди да пуснете следващото си съобщение!', 429);
        }

        return response(route('topic', ['id' => $topic_id]), 200);
    }

    public function edit($topic_id, $page = 1, $post_id) {
        $topic = new Topic($topic_id, $page);
        $entry = $topic->getEntry($post_id);
        $forum = $topic->getForum();
        $category = $forum->getCategory();

        $this->breadcrumbs->set('category', $category->getId(), $category->getName());
        $this->breadcrumbs->set('forum', $forum->getId(), $forum->getName());

        if ( $this->member->getId() != $entry->author_id || $this->member->getId() <= 0 ) {
            if ( $this->member->getGroup() != 4 ) { // ако не си админ
                return redirect()->route('topic_page', [
                    'id' => $topic_id,
                    'page' => $page
                ]);
            }
        }

        $body = new PostExtractor($entry->post);

        return view('posts.edit', [
            'topic' => $topic,
            'breadcrumbs' => $this->breadcrumbs->get(),
            'post' => json_encode($body->extractToBBCode(), JSON_UNESCAPED_UNICODE)
        ]);

    }

    public function update(Request $request, $topic_id, $page, $post_id) {
        $topic = new Topic($topic_id, $page);
        $entry = $topic->getEntry($post_id);

        if ( $this->member->getId() != $entry->author_id || $this->member->getId() <= 0 ) {
            if ( $this->member->getGroup() != 4 ) { // ако не си админ
                return redirect()->route('topic_page', [
                    'id' => $topic_id,
                    'page' => $page
                ]);
            }
        }

        $params = [
            'st' => 0,
            'f' => $topic->getDetails('forum_id'),
            't' => $topic->getId(),
            'p' => $entry->id,
            'auth_key' => (new AuthKey())->getBody(),
            'Post' => Post::encode($request->input('post')),
        ];

        $post = new Post($params, POST::EDIT_POST);
        if ($post->send() == 429) {
            return response('Моля, изчакайте преди да пуснете следващото си съобщение!', 429);
        }

        return response(route('topic', ['id' => $topic_id]), 200);
    }
}