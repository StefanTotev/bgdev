@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <section id="topic-header">
        <div class="container flex">
            <h2><a href="{{ route('topic', ['id' => $topic->getId()]) }}">{{ $topic->getName() }}</a></h2>
        </div>
    </section>
    @include('partials.pagination', ['id' => $topic->getId(), 'route' => 'topic_page'])
    <section class="post-section">
        <div class="container">
            @foreach($topic->getPosts() as $post)
                <div class="row comment" id="entry{{ $post->id }}">
                    <div class="member-card">
                        <div class="image">
                            <img src="{{ $members->getMemberByName($post->author_name)->getAvatar() }}" alt="Аватар">
                        </div>
                        <div class="content">
                            <div class="member-name" title="Име на потребител">
                                @if ( $post->author_id > 0 )
                                    <a href="{{ route('member', ['id' => $post->author_name]) }}"
                                       class="forum-sections-link {{ $members->getMemberByName($post->author_name)->getColor() }}">{{ $post->author_name}}</a>
                                    {{--@if ( array_search(['id' => $post->author_id, 'name' => $post->author_name], $online['members']) !== false )--}}
                                        {{--<i class="fa fa-circle moderator fa-xs"></i>--}}
                                    {{--@endif--}}
                                @else
                                    <span class="red">{{ $post->author_name }}</span>
                                @endif
                            </div>
                            <div class="member-group" title="Група">{{ $members->getMemberByName($post->author_name)->getTitle() }}</div>
                        </div>
                        <div class="footer">
                            <div class="comments" title="Общо мнения"><i class="fas fa-comments fa-md"></i> {{ $members->getMemberByName($post->author_name)->getPosts() }}</div>
                            <div class="joined" title="Регистриран на"><i class="fas fa-calendar-alt fa-md"></i> {{ $members->getMemberByName($post->author_name)->getJoined() }}</div>
                        </div>
                    </div> <!-- ./member-card -->
                    <div class="post">
                        <div class="header">
                            <div class="comment-posted-mobile">
                                <div>
                                    @if ( $post->author_id > 0 )
                                        <a href="{{ route('member', ['id' => $post->author_name]) }}"
                                           class="forum-sections-link {{ $members->getMemberByName($post->author_name)->getColor() }}"><i class="fa fa-user fa-sm"></i>&nbsp;{{ $post->author_name }}</a>
                                        {{--@if ( array_search(['id' => $post->author_id, 'name' => $post->author_name], $online['members']) !== false )--}}
                                        {{--<i class="fa fa-circle moderator fa-xs"></i>--}}
                                        {{--@endif--}}
                                    @else
                                        <span class="red"><i class="fa fa-user fa-sm"></i> {{ $post->author_name }}</span>
                                    @endif
                                </div>
                                <div><i class="fa fa-clock fa-sm"></i>&nbsp;{{ date('j M Y, H:i', $post->post_date) }}</div>
                            </div>
                            <div class="comment-posted" title="Дата на коментара"><i class="fa fa-clock fa-md"></i>&nbsp;{{ date('j M Y, H:i', $post->post_date) }}</div>

                            <div class="mobile-controls">
                                <a href="#" class="post-context-menu"><i class="fa fa-ellipsis-v fa-lg"></i></a>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="{{ route('post.create', ['id' => $topic->getId()]) }}" title="Отговор в темата"><i class="fa fa-reply fa-sm"></i><span class="text">Отговор</span></a>
                                    <a title="Цитирай потребител"><i class="fa fa-quote-left fa-sm"></i><span class="text">Цитат</span></a>
                                    <a title="Редактирай мнение"><i class="fa fa-edit fa-sm"></i><span class="text">Промяна</span></a>
                                </div>
                            </div>
                            <div class="controls">
                                <a href="{{ route('post.create', ['id' => $topic->getId()]) }}" class="forum-control" title="Отговор в темата"><span class="btn-text">Отговор</span><i class="fa fa-reply fa-sm"></i></a>
                                <a href="#" class="forum-control" title="Цитирай потребител"><span class="btn-text">Цитат</span><i class="fa fa-quote-left fa-sm"></i></a>
                                <a href="{{ route('post.edit', ['topic_id' => $topic->getId(), 'page' => $paginator->current(), 'entry_id' => $post->id]) }}" class="forum-control" title="Редактирай мнение"><span class="btn-text">Промяна</span><i class="fa fa-edit fa-sm"></i></a>
                            </div>
                        </div>
                        <div class="comment">
                            <div class="content">
                                {!! clean((new App\Helpers\Extractors\PostExtractor($post->post))->extract(), ['HTML.Allowed' => 'div[class],b,strong,i,em,u,a[href|title],ul,ol,li,p[style],br,span[style],img[width|height|alt|src],code,pre']) !!}
                            </div>
                        </div>
                        @if ( $members->getMemberByName($post->author_name)->getSignature() )
                        <div class="signature">
                            <div>
                                {{ $members->getMemberByName($post->author_name)->getSignature() }}
                            </div>
                        </div>
                        @endif
                    </div> <!-- ./post -->
                </div> <!-- ./row comment -->
            @endforeach
        </div> <!-- ./container flex-->
    </section>
    @include('partials.pagination', ['id' => $topic->getId(), 'route' => 'topic_page'])
@endsection