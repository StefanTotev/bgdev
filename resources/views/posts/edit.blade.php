@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <section id="topic-header">
        <div class="container flex">
            <h2><a href="{{ route('topic', ['id' => $topic->getId()]) }}">{{ $topic->getName() }}</a></h2>
        </div>
    </section>

    <div id="post-section">
        <div class="container">
            <div class="post-section-header">
                <span class="title">Нов коментар</span>
            </div>
            <div class="post-section-content">
                <div class="row">
                    <form id="new-post-form">
                        <div class="form-group">
                            <input type="text" name="title" placeholder="Заглавие на тема">
                            <input type="text" name="description" placeholder="Описание на тема">
                        </div>
                        <textarea id="editor" name="post"></textarea>
                        <button type="submit" name="submit">Изпрати</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor', {
            language:'bg',
            height: '400px',
        });

        let post = @json($post);
        post = JSON.parse(post);
        post = post.replace(new RegExp('<br>', 'g'), '\n');
        post = post.replace(new RegExp('<(\/?(b|u|i))>', 'g'), '[$1]');
        post = post.replace(new RegExp('<span style=\'font-family:([a-zA-Z]+).*\'>(.*?)<\/span>', 'g'), '[font=$1]$2[/font]');
        post = post.replace(new RegExp('<span style=\'color:([#a-z0-9]+).*\'>(.*?)<\/span>', 'gi'), '[color=$1]$2[/color]');
        post = post.replace(new RegExp('<span style=\'font-size:([0-9]+).*\'>(.*?)<\/span>', 'g'), function ( match, p1, p2 ) {
            return '[size=' + (p1 - 7) + ']' + p2 + '[/size]';
        });
        post = post.replace(new RegExp('<a href=\'(.*?)\'.*>(.*?)<\/a>', 'g'), '[url=$1]$2[/url]');
        post = post.replace(new RegExp('<img src=\'(.*?)\'.*>', 'g'), '[img]$1[/img]');
        post = post.replace(new RegExp('<li>(.*?)<\/li>', 'g'), '[*]$1');
        post = post.replace(new RegExp('<ul>(.*?)<\/ul>', 'g'), '[list]$1[/list]');
        CKEDITOR.instances.editor.setData(post);
    </script>
@endpush