<section id="navbar">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Начало</a><i class="fa fa-angle-right fa-md blue"></i></li>
            @isset($breadcrumbs)
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{ url($breadcrumb['url'], $breadcrumb['id']) }}">{{ $breadcrumb['name'] }}</a><i class="fa fa-angle-right fa-md blue"></i></li>
                @endforeach
            @endisset
        </ul>
        <div class="mobile-breadcrumbs">
            <select>
                @isset($breadcrumbs)
                    @for($i = count($breadcrumbs)-1; $i >= 0; $i--)
                        <option value="{{ url($breadcrumbs[$i]['url'], $breadcrumbs[$i]['id']) }}">{{ $breadcrumbs[$i]['name'] }}<i class="fa fa-angle-right fa-md blue"></i></option>
                    @endfor
                @endisset
                <option value="{{ route('home') }}">Начало</option>
            </select>
        </div>
        @if ($member->isLogged())
        <ul class="submenu">
            <li><a href="#">Потребители</a></li>
            <li><a href="#">Контролен панел</a></li>
            <li><a href="#">Съобщения</a></li>
        </ul>
        @endif
    </div>
</section>