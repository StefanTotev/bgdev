@extends('partials.modals.modal')

@section('type', 'login')

@section('modal-content')
    @if ( $member->getId() <= 0)
        <div class="modal-header">
            <span class="btn-close"><i class="fa fa-times"></i></span>
            <span class="modal-header-title">Вход</span>
        </div>
        <div class="modal-body">
            <form action="{{ route('login') }}" method="POST">
                <div class="form-group">
                    <label for="username">Потребителско име</label>
                    <input type="text" name="username" id="username" class="form-input" placeholder="Потребителско име" />
                </div>
                <div class="form-group">
                    <label for="password">Парола</label>
                    <input type="password" name="password" id="password" class="form-input" placeholder="Парола" />
                </div>
                <div class="rememberme-group">
                    <input type="checkbox" name="rememberme" id="rememberme" class="form-checkbox" />
                    <label for="rememberme">Запомни ме!</label>
                </div>
                <div class="rememberme-group">
                    <input type="checkbox" name="privacy" id="privacy" class="form-checkbox" />
                    <label for="privacy">Влез анонимно!</label>
                </div>
                <input type="submit" name="submit" class="form-btn" value="Вход" />
            </form>
        </div>
    @endif
@endsection