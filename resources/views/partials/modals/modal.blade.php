<!-- The Modal -->
<div id="@yield('type')" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        @yield('modal-content')
    </div>
</div>