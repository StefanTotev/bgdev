<section class="forum-section">
    <div class="container">
        <div class="forum-section-header">
            <span>Под форуми</span>
            <div class="controls">
                <span class="section-toggle-btn"><i class="fa fa-angle-down fa-lg"></i></span>
            </div>
        </div>
        <div class="forum-section-content">
            @foreach($forum->getSubForums() as $subforum)
                <div class="row">
                    <div class="icon"><i class="fas fa-lock fa-2x red"></i></div>
                    <div class="title">
                        <span class="subforum-name"><a href="{{ route('forum', ['id' => $subforum->id]) }}" class="forum-sections-link">{{ $subforum->name }}</a></span>
                        <span class="stats-mobile">{{ $subforum->topics }} Теми {{ $subforum->posts }} Мнения</span>
                        <div class="description">{{ $subforum->description }}</div>
                        <div class="last-poster-mobile">
                        <span>
                            <a href="{{ route('topic', ['id' => $subforum->last_id]) }}" class="forum-sections-link" title="Към последното мнение">
                                {{ $subforum->last_title }}
                            </a>
                            &nbsp;</span>
                            <span>от
                                @if ( $subforum->last_poster_id > 0)
                                    <a href="{{ url('member', $subforum->last_poster_name) }}"
                                       class="forum-sections-link {{ $members->getMemberByName($subforum->last_poster_name)->getColor() }}"
                                       title="Виж профила">
                                        {{ $subforum->last_poster_name }}
                                    </a>,
                                @endif
                                </span>
                            <span>&nbsp;<b>{{ date('j M Y, H:i', $subforum->last_post) }}</b></span>
                        </div>
                    </div>
                    <div class="stats">
                        <div>{{ $subforum->topics }} теми</div>
                        <div>{{ $subforum->posts }} мнения</div>
                    </div>
                    <div class="last-poster">
                        <div>
                            @if(!empty($subforum->last_title))
                                <a href="{{ route('topic', ['id' => $subforum->last_id]) }}" class="forum-sections-link" title="Към последното мнение">
                                    <i class="fa fa-angle-right"></i>
                                    {{ $subforum->last_title }}
                                </a>
                            @else
                                <span><i class="fa fa-angle-right"></i> Няма теми</span>
                            @endif
                        </div>
                        @if ( $subforum->last_poster_id > 0 )
                        <div>
                            <a href="{{ url('member', $subforum->last_poster_name) }}"
                               class="forum-sections-link {{ $members->getMemberByName($subforum->last_poster_name)->getColor() }}"
                               title="Виж профила">
                                <i class="fa fa-user fa-xs"></i> {{ $subforum->last_poster_name }}
                            </a>
                        </div>
                        <div><i class="fa fa-clock fa-sm"></i> {{ date('j M Y, H:i', $subforum->last_post) }}</div>
                        @endif
                    </div>
                </div> <!-- ./row -->
            @endforeach
        </div> <!-- ./forum-section-content -->
    </div> <!-- ./container -->
</section>