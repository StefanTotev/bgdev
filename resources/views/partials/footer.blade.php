<footer>
    <div class="container flex">
        <div class="box">
            <h3>Статистика</h3>
            <div>Потребителите ни са публикували общо <b>{{ ($status->status->total_replies + $status->status->total_topics) }}</b> мнения</div>
            <div>Имаме <b>{{ $status->status->mem_count }}</b> регистрирани потребители</div>
            <div>
                Най-новият потребител е
                <a href="{{ url('member', $status->status->last_member_name) }}" class="forum-sections-link member">
                    <b>{{ $status->status->last_member_name }}</b>
                </a>
            </div>
            <div>Най-голямата посещаемост на форума (<b>{{ $status->status->most_count }}</b> потребители) беше на <b>{{ date('j M Y H:iч.', $status->status->most_date) }}</b></div>
        </div>
        <div class="box">
            <h3>Кой е онлайн?</h3>
            <div>
                Общо потребители на линия:
                {{ $online['guests'] }} Гости,
                {{ count($online['members']) }} Регистрирани,
                {{ $online['anon'] }} Анонимни
            </div>
            <div>
                Регистрирани потребители:
                @forelse($online['members'] as $member)
                    <a href="{{ route('member', ['id' => $member->getId()]) }}" class="forum-sections-link {{ $member->getColor() }}">{{ $member->getName() }}</a><span class="comma">&comma;</span>
                @empty
                    Няма
                @endforelse
            </div>
        </div>
    </div>
</footer>
