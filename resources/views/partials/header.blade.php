<header>
    <div class="container flex">
        <h1><a href="{{ url('/') }}"><span class="orange">BG</span><span class="member">Dev</span></a></h1>
        <nav>
            <a href="#" id="mobile-nav"><i class="fa fa-bars fa-lg"></i></a>
            <ul id="top-navigation">
                @if($member->getId() > 0)
                    <li><a href="{{ route('member', ['id' => $member->getName()]) }}" class="top-nav-link">{{ $member->getName() }}</a></li>
                    <li><a href="{{ route('logout') }}" class="top-nav-link">Изход</a></li>
                @else
                    <li><a style="cursor: pointer;" class="top-nav-link" data-target="login">Вход</a></li>
                    <li><a style="cursor: pointer;" class="top-nav-link">Регистрация</a></li>
                @endif
                {{--<li><a href="#" class="top-nav-link"><i class="fa fa-search"></i></a></li>--}}
            </ul>
        </nav>
    </div>
</header>