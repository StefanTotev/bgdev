@if ( $paginator->count() > 1 )
<div id="pagination">
    <div class="container flex">
        <ul>
            <li><a href="{{ route(explode('_', $route)[0], ['id' => $id]) }}" class="link"><i class="fa fa-angle-double-left"></i></a></li>
            <li><a href="{{ route($route, ['id' => $id, 'page' => $paginator->previous()]) }}" class="link"><i class="fa fa-angle-left"></i></a></li>
            <div>
            @for ( $i = $paginator->start(); $i <= $paginator->end(); $i++ )
                @if ( $i == $paginator->current() )
                    <li><a href="{{route($route, ['id' => $id, 'page' => $i]) }}" class="current">{{ $i }}</a></li>
                @else
                    <li><a href="{{route($route, ['id' => $id, 'page' => $i]) }}" class="link">{{ $i }}</a></li>
                @endif
            @endfor
            </div>
            <li><a href="{{ route($route, ['id' => $id, 'page' => $paginator->next()]) }}" class="link"><i class="fa fa-angle-right"></i></a></li>
            <li><a href="{{ route($route, ['id' => $id, 'page' => $paginator->last()]) }}" class="link"><i class="fa fa-angle-double-right"></i></a></li>
        </ul>
    </div>
</div>
@endif