<div id="sidebar">
    <div class="sidebar-content">
        <div id="sidebar-title">
            @if($member->isLogged())
                {{ $member->getName() }}
            @else
                Меню
            @endif
        </div>
        <ul>
            <li><a href="#">Потребители</a></li>
            @if( $member->isLogged() )
                <li><a href="#">Контролен панел</a></li>
                <li><a href="#">Съобщения</a></li>
                <li><a href="{{ route('logout') }}">Изход</a></li>
            @else
                <li><a data-target="login">Вход</a></li>
            @endif
        </ul>
        <a id="bottom-close-btn"><i class="fas fa-arrow-right fa-2x"></i></a>
    </div>
</div>