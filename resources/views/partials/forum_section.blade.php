<section class="forum-section">
    <div class="container">
        <div class="forum-section-header">
            <a href="{{ url('category', $category->getId()) }}">{{ $category->getName() }}</a>
            <div class="controls">
                <span class="section-toggle-btn"><i class="fa fa-angle-down fa-lg"></i></span>
            </div>
        </div>
        <div class="forum-section-content">
            @foreach($category->getForums() as $forum)
                <div class="row">
                    <div class="icon"><i class="{{ ($forum->status == '1') ? 'far fa-comment orange' : 'fas fa-lock red' }} fa-2x"></i></div>
                    <div class="title">
                        <span class="subforum-name"><a href="{{ route('forum', ['id' => $forum->id]) }}" class="forum-sections-link">{{ $forum->name }}</a></span>
                        <span class="stats-mobile">{{ $forum->topics }} Теми {{ $forum->posts }} Мнения</span>
                        <div class="description">{{ $forum->description }}</div>
                        <div class="last-poster-mobile">
                            <span>
                                <a href="{{ route('topic', ['id' => $forum->last_id]) }}" class="forum-sections-link" title="Към последното мнение">{!! $forum->last_title !!}</a>
                            </span>
                            <span>от
                                @if ( $forum->last_poster_id > 0 )
                                <a href="{{ route('member', ['id' => $forum->last_poster_name]) }}"
                                   class="forum-sections-link {{ $members->getMemberByName($forum->last_poster_name)->getColor() }}"
                                   title="Виж профила">{{ $forum->last_poster_name }}</a>,
                                @endif
                            </span>
                            <span>&nbsp;<b>{{ date('j M Y, H:i', $forum->last_post) }}</b></span>
                        </div>
                    </div>
                    <div class="stats">
                        <div>{{ $forum->topics }} теми</div>
                        <div>{{ $forum->posts }} мнения</div>
                    </div>
                    <div class="last-poster">
                        <div>
                            @if(!empty($forum->last_title))
                            <a href="{{ route('topic', ['id' => $forum->last_id]) }}" class="forum-sections-link" title="Към последното мнение">
                                <i class="fa fa-angle-right"></i>
                                {!! $forum->last_title !!}</a>
                            @else
                                <span><i class="fa fa-angle-right"></i> Няма теми</span>
                            @endif
                        </div>
                        <div>
                            @if ( $forum->last_poster_id > 0 )
                            <a href="{{ route('member', ['id' => $forum->last_poster_name]) }}"
                               class="forum-sections-link {{ $members->getMemberById($forum->last_poster_id)->getColor() }}"
                               title="Виж профила">
                                <i class="fa fa-user fa-xs"></i> {{ $forum->last_poster_name }}</a>
                            @else
                                <span class="{{ $members->getMemberById($forum->last_poster_id)->getColor() }}"><i class="fa fa-user fa-xs"></i> {{ $forum->last_poster_name }}</span>
                            @endif
                        </div>
                        <div><i class="fa fa-clock fa-sm"></i> {{ date('j M Y, H:i', $forum->last_post) }}</div>
                    </div>
                </div> <!-- ./row -->
            @endforeach
        </div> <!-- ./forum-section-content -->
    </div> <!-- ./container -->
</section>