@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    @if ( $member->getId() > 0 && count($forum->getSubForums()))
        @include('partials.sub_forums', ['forum' => $forum])
    @endif
    @include('partials.pagination', ['id' => $forum->getId(), 'route' => 'forum_page'])
    <section class="forum-section">
        <div class="container">
            <div class="forum-section-header">
                <span class="title">{{ $forum->getName() }}</span>
                <div class="controls">
                    <a href="{{ route('topic.create', ['id' => $forum->getId()]) }}" class="forum-control">Нова тема</a>
                    <a href="#" class="forum-control">Анкета</a>
                </div>
            </div>
            <div class="forum-section-content">
                @foreach($forum->getTopics() as $topic)
                    <div class="row">
                        <div class="icon"><i class="{{ ($topic->state != 'closed') ? 'far fa-comment orange' : 'fas fa-lock red' }} fa-2x"></i></div>
                        <div class="title">
                            <span class="subforum-name">
                                @if ( $topic->pinned )
                                    <i class="fas fa-map-pin fa-lg red"></i>
                                @endif
                                <a href="{{ route('topic', ['id' => $topic->id]) }}" class="forum-sections-link">{!! $topic->title !!}</a>
                            </span>
                            <span class="stats-mobile">{{ $topic->posts }} мнения {{ $topic->views }} прегледа</span>
                            <div class="description">
                                <span class="topic-info">
                                    <i class="fa fa-star fa-sm"></i>
                                    @if ( $topic->starter_id > 0 )
                                        <a href="{{ route('member', ['id' => $topic->starter_name]) }}" class="forum-sections-link {{ $members->getMemberByName($topic->starter_name)->getColor() }}"> {{ $topic->starter_name }}</a>
                                    @else
                                        <span class="red">{{ $topic->starter_name }}</span>
                                    @endif
                                </span>
                                <span class="topic-info"><i class="fa fa-clock fa-sm"></i> {{ date('j M Y, H:i', $topic->start_date) }}</span>
                            </div>
                            <div class="last-poster-mobile">
                                <span class="topic-info">
                                    <i class="fa fa-angle-double-right"></i>
                                    @if ( $topic->last_poster_id > 0 )
                                        <a href="{{ route('member', ['id' => $topic->last_poster_name]) }}"
                                           class="forum-sections-link {{ $members->getMemberByName($topic->last_poster_name)->getColor() }}"> {{ $topic->last_poster_name }}</a>
                                    @else
                                        <span class="red"> {{ $topic->last_poster_name }}</span>
                                    @endif
                                </span>
                                <span class="topic-info">
                                <a href="{{ route('topic', ['id' => $topic->id]) }}" class="forum-sections-link">
                                    <i class="fa fa-clock fa-sm"></i> {{ date('j M Y, H:i', $topic->last_post) }}</a>
                            </span>
                            </div>
                        </div>
                        <div class="stats">
                            <div>{{ $topic->posts }} мнения</div>
                            <div>{{ $topic->views }} прегледа</div>
                        </div>
                        <div class="last-poster">
                            <div>
                                @if ( $topic->last_poster_id > 0 )
                                <a href="{{ route('member', ['id' => $topic->last_poster_name]) }}"
                                    class="forum-sections-link {{ $members->getMemberByName($topic->last_poster_name)->getColor() }}"><i class="fa fa-user fa-xs"></i> {{ $topic->last_poster_name }}</a>
                                @else
                                    <span class="red"><i class="fa fa-user fa-xs"></i> {{ $topic->last_poster_name }}</span>
                                @endif
                            </div>
                            <div><a href="{{ route('topic', ['id' => $topic->id]) }}" class="forum-sections-link"><i class="fa fa-clock fa-sm"></i> {{ date('j M Y, H:i', $topic->last_post) }} <i class="fa fa-angle-double-right"></i></a></div>
                        </div>
                    </div> <!-- ./row -->
                @endforeach
            </div> <!-- ./forum-section-content -->
        </div> <!-- ./container -->
    </section>
    @include('partials.pagination', ['id' => $forum->getId(), 'route' => 'forum_page'])
@endsection
