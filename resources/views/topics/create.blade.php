@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div id="post-section">
        <div class="container">
            <div class="post-section-header">
                <span class="title">Нов тема&#60;</span>
            </div>
            <div class="post-section-content">
                <div class="row">
                    <form id="new-post-form">
                        <div class="form-group">
                            <input type="text" name="title" placeholder="Заглавие на тема">
                            <input type="text" name="description" placeholder="Описание на тема">
                        </div>
                        <textarea id="editor" name="post"></textarea>
                        <button type="submit" name="submit">Изпрати</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor', {
            language:'bg',
            height: '400px',
        });
    </script>
@endpush