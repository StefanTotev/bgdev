@inject('categories', 'App\Categories')

@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs')
@endsection

@section('content')
    @foreach($categories->getList() as $category)
        @include('partials.forum_section', ['category' => $category])
    @endforeach
@endsection
