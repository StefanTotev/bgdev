@extends('layouts.master')

@section('breadcrumbs')
    @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    @include('partials.forum_section', ['category' => $category])
@endsection