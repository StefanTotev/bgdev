## Изисквания ##
* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension

*С версията са си активирани по подразбиране*

## Инсталация ##

```
#!bash
cd /path/to/install
git clone https://StefanTotev@bitbucket.org/StefanTotev/bgdev.git .
composer install --no-dev
```

## Конфигурация ##

### 1. Задаване на root директория ###
Трябва да се промени root директорията да води към **/public**

### 2. Преименуваме конфигурационния файл ###

```
#!bash
cp .env.example .env
```

### 3. Променяме малко настройки(ако е наложително) ###

```
#!bash
APP_ENV="BG Dev Форуми"
APP_DEBUG=true
APP_KEY=SYLriu7KDG1Izi2WTPtr6Rah5Jdn79j9
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

CACHE_DRIVER=file
QUEUE_DRIVER=sync

API_URI="http://bgdev.saas.bg/forums/api"
```

* **APP_ENV** - го използвам за статичната част на заглавието на сайта
* **APP_DEBUG** - ще е хубаво да спрем всякакъв debug
* **APP_TIMEZONE** - за България мисля че трябва да е UTC+2
* **API_URI** - е URL-а, където работи самото API


### 4. Права за писане ###
Уверете се, че **storage** папката е с права за писане